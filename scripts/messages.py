from mappings import LINKS

SEARCH_GAME = """
Zapraszanie na inne serwery może skutkować banem. Prosimy o wysyłanie wiadomości dopiero po stworzeniu kanału głosowego o treści: 

**link.do/kanału_vc KOD - liczba potrzebnych osób**
*przykład:*

https://discord.gg/uWZh9sc XYZA - potrzebujemy 2 osób
"""
VOTER_MESSAGES = {
    "pl": "Hej, właśnie skończył Ci się kolor premium! Zagłosuj ponownie aby odnowić: https://top.gg/servers/960665311701528596/vote",
    "en": "Hey, you've just run out of premium color! Vote again to renew: https://top.gg/servers/960665311701528596/vote",
}

VC_PERMISSION_MESSAGE = {
    "pl": f' chcesz uzyskać dostęp do tej komendy? Sprawdź, jak łatwo możesz zdobyć premium <:mastercard:1270433919233425545> przez {LINKS["tipply"]} i ciesz się dodatkowymi korzyściami! Szczegóły dostępnych ról i opcji znajdziesz na kanale <#960665316109713421>."',
    "en": f' you must have premium <:mastercard:1270433919233425545> {LINKS["tipply"]}, boost server or invite 4 people to the server. `?profile`',
}

JOIN_GAME_MESSAGES = {"pl": " Dołącz na kanał głosowy: ", "en": " Join voice channel: "}

BOT_HACK = {
    "pl": "UWAGA! Nasz bot wykrył, że Twoje konto discord zostało zainfekowane! "
    "Jak najszybciej zmień hasło do discorda! Następnie będziesz mógł wrócić na serwer "
    "z którego zostałeś tymczasowo wyrzucony: "
    "https://discord.gg/zagadka"
}


SAY_HC = """
**Rangi numeryczne [1, 2, 3 … 128] na liście użytkowników. Co to jest?**
Jest to autorski system rankingu aktywności, stworzony na potrzeby tego serwera.

**Jak zdobyć własną rangę na liście użytkowników? Sposobów jest kilka:**
1. Aktywność na czacie tekstowym.
2. Przebywanie na kanałach głosowych.
3. Aktywność osób, które zostały przez Ciebie zaproszone (10% wygenerowanego ruchu pojawia się też na Twoim koncie)
4. Nitro Serwer Boost lub Patronite (https://www.patreon.com/zaGadka), zwiększają wszystkie pozostałe bonusy o 25%

**Co dostaniesz za zaproszenie nowej osoby na serwer?** (ilość zaproszonych osób `?profile`)
1+ osoba ♳ - czarny kolor nicku do końca dnia
2 osoby ♴ - DJ, czyli kontrola nad botami muzycznymi
4 osoby ♵ - możliwość tworzenia własnego kanału głosowego z możliwością mutowania (więcej komend: `?help`)
8+ osób ♶ - +25%  punktów do końca dnia
16 osób ♷ - +25% punktów na stałe
**32 osoby ✪ - osoba po uzyskaniu danej liczby zaproszeń, jest pierwszej kolejności brana pod uwagę, przy wyborze moderatora**
64+ osoby ♸ - indywidualna ranga, możesz zmieniać jej kolor za pomocą komendy np `?color light blue` do końca dnia
128+ osób ♹ - indywidualna ranga jak wyżej, z tą różnicą że cyklicznie zmienia ona wybrane przez Ciebie kolory
256 osób ⚀ -dostep do kanalu premium, +25% punktów na stałe
512, 1024, 2048 ⚁⚂⚃ - +25% punktów na stałe + **nitro** (lub równowartość pln)
+ oznacza, że w następnym dniu, rola się odświeży po dołączeniu jednej osoby

**Jak zmienić kolor nicku?**
1. Wybrać <a:zaGadkaRed:592406448302981149> <a:zaGadkaGreen:592406448315564062> <a:zaGadjaBlue:592406448386998289> za pomocą reakcji widocznych na samym dole
2. Aby otrzymać wyjątkowy kolor, należy zaprosić nową osobę (czarny), boostować serwer (czarny), być patronem ($2- czarny, $4- dowolny)
"""

JOIN_MESSAGE = """
Cześć {member_name}, witaj na serwerze {guild_name}!

**Co zrobić aby otrzymać wyróżniający się nick?**
czarny - zaproś znajomych na serwer
żółty - zagłosuj https://top.gg/servers/960665311701528596/vote (kilka sekund roboty)

więcej informacji na <#960665311965749294>
"""
