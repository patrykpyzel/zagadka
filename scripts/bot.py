import asyncio
import inspect
import io
import json
import re
from collections import Counter, defaultdict
from datetime import datetime, timedelta, timezone
from random import choice, choices, randint

import aiohttp
import datasources.models as models
import discord
import functions as f
import messages as msg
import requests
from colour import Color
from datasources import engine, session
from datasources.queries import *
from mappings import (
    BANNED_LINKS,
    BANNED_WORDS,
    BONUS,
    BONUS_2,
    BOT,
    CHANNELS,
    CHANNELS_REACTIONS,
    COMMANDS,
    DISBOARD,
    EMOJIS,
    GAMES,
    GUILD,
    LETTER_LENGTH,
    LINKS,
    MUSIC_COMMANDS,
    MUSIC_PREFIX,
    SPAMBOT_TEXT,
    VC_ID_TO_MAX,
    WATERMARK,
)
from PIL import Image, ImageDraw, ImageFont

# if not engine.dialect.has_table(engine, 'member'):
#  models.Base.metadata.create_all(engine)
# intents = discord.Intents()
# intents.all()
intents = discord.Intents.all()
client = discord.Client(intents=intents)
guild_glob = None
invites = []
colors = {}
channels = {}
public_channels = {}
daily_ban_limit = {}
hooks = {}
temp_bans = {}
temp_x = defaultdict(int)
bot_datetime = get_guild_date(GUILD["id"])
print("Bot datetime: ", bot_datetime)
bump_datetime = datetime.now() + timedelta(hours=2)
game_create_channels = [game["create_channel"] for game in GAMES.values()]
game_join_randoms = [game["join_random"] for game in GAMES.values()]
game_roles = [game["role"] for game in GAMES.values()]
game_categories = [game["category"] for game in GAMES.values()]
change_banner = False


# change_avatar = True


def is_category_relevant(category):
    # Sprawdza czy puste kanały mają być usuwane w tej kategorii
    return category.id in GUILD["monitored_categories"]


def is_public_max_category(category):
    # Sprawdza czy kanały mają mieć nazwę public/max czy nick
    return category.id in GUILD["public_max_categories"]


def count_empty_channels_without_star(category, current_channel):
    count = 0
    for channel in category.voice_channels:
        if channel == current_channel:
            continue
        if len(channel.members) == 0 and "✩" not in channel.name:
            count += 1
    return count


def get_user_limit_from_first_channel(category):
    return category.voice_channels[0].user_limit if category.voice_channels else None


@client.event
async def on_voice_state_update(member, before, after):
    guild = member.guild
    nsfw = guild.get_role(GUILD["video_ban"])
    mute = guild.get_role(GUILD["mute_id"])
    if after.channel and (before.channel != after.channel):
        if after.channel.id in GUILD["create_channels"]:
            overwrite = get_member_permissions(member.id)
            permission_overwrites = {}
            overwrite_guests = get_member_guests(member.id)
            for guest_id, view_channel, connect, speak in overwrite_guests:
                guest = guild.get_member(guest_id)
                if guest:
                    if (
                        all(p is None for p in [view_channel, connect, speak])
                        or guest_id == member.id
                    ):
                        continue
                    else:
                        permission_overwrites[guest] = discord.PermissionOverwrite(
                            read_messages=view_channel, connect=connect, speak=speak
                        )

            first95 = {
                k: permission_overwrites[k] for k in list(permission_overwrites)[:95]
            }
            first95[guild.default_role] = discord.PermissionOverwrite(
                read_messages=overwrite.host_everyone_view_channel,
                connect=overwrite.host_everyone_connect,
                speak=overwrite.host_everyone_speak,
            )
            first95[member] = discord.PermissionOverwrite(
                read_messages=True, connect=True, speak=True, move_members=False
            )
            first95[nsfw] = discord.PermissionOverwrite(stream=False)
            first95[mute] = discord.PermissionOverwrite(send_messages=False)
            other_perms = {
                k: permission_overwrites[k] for k in list(permission_overwrites)[95:]
            }
            channel_name = member.display_name
            for word in BANNED_WORDS:
                if word in member.display_name.lower():
                    channel_name = "?boostować serwer??"
                    break
            try:
                new_channel = await guild.create_voice_channel(
                    "-" + channel_name,
                    category=after.channel.category,
                    bitrate=guild.bitrate_limit,
                    user_limit=overwrite.host_channel_limit,
                    overwrites=first95,
                )
            except discord.errors.HTTPException:
                new_channel = await guild.create_voice_channel(
                    "- ???",
                    category=after.channel.category,
                    bitrate=guild.bitrate_limit,
                    user_limit=overwrite.host_channel_limit,
                    overwrites=first95,
                )
            try:
                await member.move_to(new_channel)
            except (discord.errors.NotFound, discord.errors.HTTPException):
                await new_channel.delete()
                return
            channels[new_channel.id] = member.id

            for member, perms in other_perms.items():
                try:
                    await new_channel.set_permissions(member, overwrite=perms)
                except discord.errors.NotFound:
                    return
        elif after.channel.id == GUILD["afk_channel_id"]:
            return
        elif after.channel.id in CHANNELS:
            empty_channel_found = False
            has_vc_control_role = any(
                role_id in {role.id for role in member.roles}
                for role_id in GUILD["vc_control"]
            )

            # Ustal nazwę kanału
            if is_public_max_category(after.channel.category):
                if after.channel.id in VC_ID_TO_MAX:
                    channel_name = (
                        choice(EMOJIS) + " max" + VC_ID_TO_MAX[after.channel.id]
                    )
                else:
                    channel_name = choice(EMOJIS) + " public"

                # Dodaj gwiazdkę tylko dla kanałów public/max jeśli użytkownik ma odpowiednią rolę
                if has_vc_control_role:
                    channel_name += " ✩"
            else:
                channel_name = (
                    "-" + member.display_name
                )  # Bez gwiazdki dla kanałów prywatnych

            permission_overwrites = {
                nsfw: discord.PermissionOverwrite(stream=False),
                mute: discord.PermissionOverwrite(send_messages=False),
            }

            # Użytkownik z rolą z GUILD["vc_control"]
            if has_vc_control_role or not any(
                len(channel.members) == 0 and not channel.name.startswith("+")
                for channel in after.channel.category.voice_channels
            ):
                new_channel = await guild.create_voice_channel(
                    channel_name,
                    category=after.channel.category,
                    bitrate=guild.bitrate_limit,
                    user_limit=CHANNELS[after.channel.id][1],
                    overwrites=permission_overwrites,
                )
                try:
                    await member.move_to(new_channel)
                    public_channels[
                        new_channel.id
                    ] = member.id  # Dodajemy kanał do public_channels
                except (discord.errors.NotFound, discord.errors.HTTPException):
                    await new_channel.delete()
                    return

            # Inni użytkownicy
            else:
                empty_channel_found = True
                for channel in after.channel.category.voice_channels:
                    if len(channel.members) == 0 and not channel.name.startswith("+"):
                        try:
                            await member.move_to(channel)
                            public_channels[
                                channel.id
                            ] = member.id  # Dodajemy kanał do public_channels
                            empty_channel_found = False
                            break
                        except (discord.errors.NotFound, discord.errors.HTTPException):
                            continue

            # Jeśli nie znaleziono pustego kanału, twórz nowy z odpowiednimi uprawnieniami
            if empty_channel_found:
                new_channel = await guild.create_voice_channel(
                    channel_name,
                    category=after.channel.category,
                    bitrate=guild.bitrate_limit,
                    user_limit=CHANNELS[after.channel.id][1],
                    overwrites=permission_overwrites,
                )
                try:
                    await member.move_to(new_channel)
                    public_channels[
                        new_channel.id
                    ] = member.id  # Dodajemy kanał do public_channels
                except (discord.errors.NotFound, discord.errors.HTTPException):
                    await new_channel.delete()
                    return

    if (
        before.channel
        and before.channel != after.channel
        and before.channel.type == discord.ChannelType.voice
        and len(before.channel.members) == 0
        and (before.channel.id not in CHANNELS)
        and (before.channel.id not in GUILD["stat_ids"])
        and (before.channel.id not in GUILD["ignore"])
    ):
        if is_category_relevant(before.channel.category):
            if "✩" in before.channel.name:
                try:
                    await before.channel.delete()
                except discord.errors.NotFound:
                    print(
                        before.channel.id,
                        "channel not found - on leave with star in relevant category",
                    )
            else:
                # Dla kanałów publicznych sprawdzamy liczbę pustych kanałów
                if is_public_max_category(before.channel.category):
                    empty_count = count_empty_channels_without_star(
                        before.channel.category, before.channel
                    )
                    if (
                        empty_count > 3
                    ):  # Usuwamy tylko gdy jest więcej niż 3 puste kanały
                        try:
                            await before.channel.delete()
                        except discord.errors.NotFound:
                            print(
                                before.channel.id,
                                "channel not found - on leave without star in public category",
                            )
                # Dla kanałów prywatnych usuwamy zawsze gdy są puste
                else:
                    try:
                        await before.channel.delete()
                    except discord.errors.NotFound:
                        print(
                            before.channel.id,
                            "channel not found - on leave from private category",
                        )


@client.event
async def on_member_join(member):
    guild = member.guild
    member_hosts = get_member_hosts(member.id)
    member_roles = get_member_roles(member.id)

    if member_roles:
        for entry in member_roles:
            if entry.active:
                new_role = guild.get_role(entry.role_id)
                await member.add_roles(new_role, reason="on member join")

    if member_hosts:
        for host_id in member_hosts:
            host = guild.get_member(host_id.member_host)
            if host and any(
                role_id in {role.id for role in host.roles}
                for role_id in GUILD["vc_control"]
            ):
                temp_channels = {k: v for k, v in channels.items() if v}
                for channel_id in temp_channels:
                    if host.id == temp_channels[channel_id]:
                        channel = guild.get_channel(channel_id)
                        await channel.set_permissions(
                            member,
                            speak=host_id.speak,
                            connect=host_id.connect,
                            read_messages=host_id.view_channel,
                        )
                temp_channels_2 = {k: v for k, v in public_channels.items() if v}
                for channel_id in temp_channels_2:
                    if host.id == temp_channels_2[channel_id]:
                        channel = guild.get_channel(channel_id)
                        await channel.set_permissions(
                            member,
                            speak=host_id.speak,
                            connect=host_id.connect,
                            read_messages=host_id.view_channel,
                        )

    invites_old = invites.copy()
    invites.clear()
    invites.extend(await member.guild.invites())
    try:
        invite = discord.utils.find(
            lambda inv: inv.uses > discord.utils.get(invites_old, id=inv.id).uses,
            invites,
        )
    except AttributeError:
        diff = list(set(invites) - set(invites_old))
        if len(diff) > 0:
            diff.sort(key=lambda inv: inv.created_at, reverse=True)
            invite = diff[0]
        else:
            invite = None

    if invite and (invite.inviter.id in temp_bans):
        try:
            await guild.system_channel.send("<@{}> autobanned".format(member.id))
            await guild.ban(member, reason=f'{guild.name} (ban_all): {"join autoban"}')
            return
        except discord.errors.NotFound:
            return

    if check_member(member.id) is False:
        if invite and invite.code != "EEzSEXU":
            set_member(
                member.id,
                member.name,
                member.discriminator,
                invite.inviter.id,
                datetime.now(),
                invite.inviter.id,
                datetime.now(),
            )
            inviter = member.guild.get_member(invite.inviter.id)
            if (member.joined_at - member.created_at) > timedelta(
                days=GUILD["join_days"]
            ):
                if inviter:
                    if not inviter.bot:
                        await inviter.add_roles(
                            member.guild.get_role(GUILD["recruiter"]),
                            reason="recruiter",
                        )
                        if (member.joined_at - member.created_at) > timedelta(
                            days=GUILD["join_days"]
                        ):
                            invited_list = get_invited_list(inviter.id)
                            real_count = 0
                            for invited_id in invited_list:
                                invited = guild.get_member(invited_id)
                                if invited:
                                    if invited.avatar and (
                                        datetime.now() - invited.created_at
                                    ) > timedelta(days=GUILD["join_days"]):
                                        real_count += 1
                            if real_count > 1:
                                await inviter.add_roles(
                                    member.guild.get_role(GUILD["dj_id"]), reason="dj"
                                )
                            if real_count > 3:
                                await inviter.add_roles(
                                    member.guild.get_role(GUILD["join_id"]),
                                    reason="join",
                                )
                            if real_count > 7:
                                await inviter.add_roles(
                                    member.guild.get_role(GUILD["temp_bonus_id"]),
                                    reason="bonus",
                                )
                            if real_count > 15:
                                await inviter.add_roles(
                                    member.guild.get_role(GUILD["bonus_id"]),
                                    reason="bonus",
                                )
                            if real_count > 31:
                                pass
                            # if (real_count > 63) and (real_count < 128):
                            if real_count > 63:
                                if any(
                                    role.name == GUILD["colored_name"]
                                    for role in inviter.roles
                                ):
                                    pass
                                else:
                                    colored_role = await guild.create_role(
                                        name=GUILD["colored_name"]
                                    )
                                    colored_role_position = guild.get_role(
                                        GUILD["colored_role_position"]
                                    )
                                    print(
                                        "role position", colored_role_position.position
                                    )
                                    await inviter.add_roles(
                                        colored_role, reason="colored"
                                    )
                                    # await asyncio.sleep(10)
                                    await colored_role.edit(
                                        position=colored_role_position.position + 1,
                                        reason="position",
                                    )
                            if real_count > 127:
                                pass
                                # if any(role.name == GUILD['multi_colored_name'] for role in inviter.roles):
                                #     pass
                                # else:
                                #     multi_colored_role = await guild.create_role(name=GUILD['multi_colored_name'])
                                #     multi_colored_role_position = guild.get_role(GUILD['colored_role_position'])
                                #     await inviter.add_roles(multi_colored_role, reason='multi_colored')
                                #     # await asyncio.sleep(10)
                                #     await multi_colored_role.edit(position=multi_colored_role_position.position + 1,
                                #                                   reason='position')
                            if real_count > 255:
                                await inviter.add_roles(
                                    member.guild.get_role(BONUS["bonus_1_id"]),
                                    reason="bonus",
                                )
                            if real_count > 511:
                                await inviter.add_roles(
                                    member.guild.get_role(BONUS["bonus_2_id"]),
                                    reason="bonus",
                                )
                            if real_count > 1023:
                                await inviter.add_roles(
                                    member.guild.get_role(BONUS["bonus_3_id"]),
                                    reason="bonus",
                                )

        else:
            set_member(
                member.id,
                member.name,
                member.discriminator,
                client.user.id,
                datetime.now(),
                client.user.id,
                datetime.now(),
            )
        session.commit()
        set_member_scores(member.id, ["week"])
        session.commit()
    join_logs = member.guild.get_channel(GUILD["join_logs_id"])
    if invite:
        update_inviter(member.id, invite.inviter.id, datetime.now())
        session.commit()
        if invite.inviter.id in GUILD["no_ping_users"]:
            await join_logs.send(
                "member: {}, display_name: {}, inviter: {} {}, invite_code: {}, uses: {}".format(
                    member.mention,
                    member.display_name,
                    invite.inviter,
                    invite.inviter.id,
                    invite.code,
                    invite.uses,
                )
            )
        else:
            await join_logs.send(
                "member: {}, display_name: {}, inviter: {} <@{}>, invite_code: {}, uses: {}".format(
                    member.mention,
                    member.display_name,
                    invite.inviter,
                    invite.inviter.id,
                    invite.code,
                    invite.uses,
                )
            )
    else:
        update_inviter(member.id, client.user.id, datetime.now())
        session.commit()
        await join_logs.send(
            "member: {}, display_name: {}, inviter: {}".format(
                member.mention, member.display_name, client.user.id
            )
        )

    # await member.send(msg.JOIN_MESSAGE.format(member_name=member.name, guild_name=member.guild.name))


@client.event
async def on_message(message):
    after = message.author
    member = after
    global guild_glob
    if not guild_glob:
        guild_glob = member.guild
    if (
        message.channel.category
        and message.channel.category.id in game_categories
        and LINKS["discord"] in message.content
    ):
        content_words = message.content.split()
        for word in content_words:
            if LINKS["discord"] in word:
                code = word.split("/").pop()
                if (code.lower() != "zagadka") and (
                    code not in [invite.code for invite in invites]
                ):
                    await message.delete()
    try:
        guild = after.guild
    except AttributeError:
        return

    # date_now = datetime.now(timezone.utc)
    date_now = datetime.now()
    global bot_datetime
    global change_banner
    author = message.author
    if author.id == client.user.id:
        return
    guild = message.guild
    if guild:
        try:
            member_roles_ids = [role.id for role in author.roles]
        except AttributeError:
            return
    else:
        return

    content = message.content
    if not author.bot:
        for spam in SPAMBOT_TEXT:
            if spam in content:
                await message.delete()
                try:
                    await author.send(msg.BOT_HACK["pl"])
                except discord.errors.Forbidden as err:
                    print(err)
                if len(member_roles_ids) < 10:
                    await message.guild.kick(author, reason=f"zaGadka: {content}")
                break

    global bump_datetime
    if bump_datetime < datetime.now():
        bump_datetime = datetime.now() + timedelta(minutes=DISBOARD["interval"])
        await guild.get_channel(GUILD["bump_channel_id"]).send(DISBOARD["message_bump"])
        await guild.get_channel(GUILD["bots_channel_id"]).send(DISBOARD["message_bot"])

    if (author.id == DISBOARD["id"]) and message.embeds:
        for embed in message.embeds:
            description = embed.to_dict()["description"]
            if any(d in description for d in DISBOARD["description"]):
                bump_datetime = datetime.now() + timedelta(hours=2)

    if bot_datetime.minute != date_now.minute:
        date_db = bot_datetime
        bot_datetime = date_now

        set_guild_date(GUILD["id"], date_now)
        session.commit()
        if len(temp_bans):
            temp_bans_copy = temp_bans.copy()
            for k, v in temp_bans_copy.items():
                temp_bans[k] -= 1
                if v < 1:
                    del temp_bans[k]
        try:
            invites_new = await guild.invites()
            diff = list(set(invites_new) - set(invites))
            if len(diff) > 0:
                invites.extend(diff)
        except:
            print("guild.invites error")

        if date_now.minute % 15 == 0:
            first_role = guild.roles[1]
            if first_role.name in [GUILD["colored_name"], GUILD["multi_colored_name"]]:
                colored_role_position = guild.get_role(GUILD["colored_role_position"])
                print("role position", colored_role_position.position)
                # await asyncio.sleep(10)
                await first_role.edit(
                    position=colored_role_position.position + 1, reason="position"
                )
            if first_role.name.startswith(GUILD["guild_name"]):
                team_role_position = guild.get_role(GUILD["team_role_position"])
                print("role position", team_role_position.position)
                await first_role.edit(
                    position=team_role_position.position + 1, reason="position"
                )
            print(date_now, "15 min")
            for role in colors:
                if colors[role]:
                    try:
                        await role.edit(colour=choice(colors[role]))
                    except discord.errors.NotFound:
                        del colors[role]
            if change_banner:
                rgb = str(randint(1, 4))
                with open(f"images/banners/{rgb}.png", "rb") as file:
                    banner = file.read()
                with open(f"images/icons/{rgb}.gif", "rb") as file:
                    icon = file.read()
                await message.guild.edit(icon=icon, banner=banner)

            for channel in guild.voice_channels:
                try:
                    if (
                        (len(channel.members) == 0)
                        and (channel.id not in CHANNELS)
                        and (channel.id not in GUILD["stat_ids"])
                        and (channel.id not in GUILD["ignore"])
                        and not (
                            is_category_relevant(channel.category)
                            and "✩" not in channel.name
                        )
                    ):
                        # and (channel.id not in game_create_channels) \
                        # and (channel.id not in game_join_randoms):
                        if channels.get(channel.id):
                            del channels[channel.id]
                        try:
                            await channel.delete()
                        except discord.errors.NotFound:
                            print("no channel to del")
                        continue
                except AttributeError:
                    pass
                try:
                    for member in channel.members:
                        if (not member.bot) and (check_member(member.id) is False):
                            if (date_now - member.joined_at).seconds > 60:
                                set_member(
                                    member.id,
                                    member.name,
                                    member.discriminator,
                                    None,
                                    datetime.now(),
                                    None,
                                    datetime.now(),
                                )
                                session.commit()
                                set_member_scores(member.id, ["week"])
                                session.commit()
                        if (
                            member.voice.self_mute
                            or member.voice.self_deaf
                            or member.bot
                        ):
                            continue
                        else:
                            if len(channel.members) > 1:
                                add_member_score(member.id, date_now.strftime("%A"), 12)
                                session.commit()
                            else:
                                add_member_score(member.id, date_now.strftime("%A"), 2)
                                session.commit()
                except AttributeError:
                    continue

            for member in guild.members:
                if (
                    member.activity
                    and member.activity.name
                    and (
                        LINKS["vanity_url"] in member.activity.name
                        or "gg/zagadka" in member.activity.name
                    )
                ):
                    add_member_score(member.id, date_now.strftime("%A"), 4)
                    session.commit()
                    continue

            print("15 end")
        if date_db.strftime("%A") != date_now.strftime("%A"):
            print("nowy dzień!")
            for mod_2 in daily_ban_limit:
                daily_ban_limit[mod_2] = GUILD["ban_limit"]
            colors.clear()
            await f.clear_roles(
                guild,
                True,
                True,
                (guild.get_role(GUILD["patreon_4_id"]),),
                GUILD["colored_name"],
                GUILD["multi_colored_name"],
            )
            await f.clear_roles(
                guild,
                False,
                False,
                (),
                guild.get_role(GUILD["recruiter"]),
                guild.get_role(GUILD["temp_bonus_id"]),
            )
            reset_points_global(date_now.strftime("%A"))

        if date_now.hour != date_db.hour:
            invites_txt = [invite.code for invite in invites]
            print(date_now, date_db, "1h update")
            role_everyone = guild.get_role(GUILD["fake_everyone_id"])
            role_here = guild.get_role(GUILD["fake_here_id"])
            for member in role_everyone.members:
                await member.remove_roles(role_everyone)
                await member.remove_roles(role_here)
            members_top = get_top_members(GUILD["top"])
            roles_top = get_top_roles(GUILD["top"])

            for (role_top_id,), member_top in zip(roles_top, members_top):
                role_top = guild.get_role(role_top_id)

                member_top_id, member_top_score = member_top
                member = guild.get_member(member_top_id)
                if member is None:
                    reset_points_by_id(member_top_id)
                if member:
                    if (
                        member.activity
                        and member.activity.name
                        and ".gg/" in member.activity.name
                        and "zagadka" not in member.activity.name
                    ):
                        invtest = member.activity.name.partition(".gg/")[2]
                        codetest = invtest.split()[0]
                        if codetest and len(codetest) > 1:
                            reset_points_by_id(member_top_id)

                    for member_old in role_top.members:
                        if member_old.id == member.id:
                            continue
                        else:
                            await member_old.remove_roles(role_top, reason="top remove")
                    await member.add_roles(role_top, reason="top add")
            # for game_name, game_data in GAMES.items():
            #     await guild.get_channel(game_data['lounge'])\
            #         .send(f'{random.choice(msg.JOIN_GAME_MESSAGES)}{game_data["invite"]}'):
            # await f.add_reactions(guild, True)

            print("1h end")

    if (
        not message.author.bot
        and not GUILD["nsfw_id"] in [role.id for role in message.author.roles]
        and message.channel.id not in WATERMARK["ignored_channels"]
        and any(x in GUILD["roles_colors"] for x in member_roles_ids)
    ):
        for word in BANNED_LINKS:
            if word in message.content:
                return
        imgs = {}
        make_watermark = False
        contents = message.content.split()
        for item in reversed(contents):
            if (
                item.startswith("https://")
                and "discord" in item
                and item.lower().endswith(WATERMARK["filenames"])
            ):
                async with aiohttp.ClientSession() as session_aio:
                    async with session_aio.get(item) as r:
                        if r.status == 200:
                            img_bytes = io.BytesIO(await r.read())
                            print(img_bytes)
                            imgs[
                                f"discord-gg-zagadka_{message.author.id}_{datetime.now()}.png"
                            ] = img_bytes
                            # imgs[str(message.author.id) + str(datetime.now()) + '.png'] = img_bytes
                            make_watermark = True
                            contents.remove(item)
        if message.attachments:
            for attachment in message.attachments:
                if attachment.filename.endswith(WATERMARK["filenames"]):
                    image = await attachment.read()
                    img_bytes = io.BytesIO(image)
                    imgs[
                        f"discord-gg-zagadka_{message.author.id}_{datetime.now()}.png"
                    ] = img_bytes
                    # imgs[str(message.author.id) + str(datetime.now()) + '.png'] = img_bytes
                    make_watermark = True
        if make_watermark:

            def watermark(img_to_watermark, avatar):
                # print(avatar)
                paste_av = False
                size = img_to_watermark.size
                watermark = Image.new("RGB", (size[0], 100), (54, 57, 62, 255))
                draw = ImageDraw.Draw(watermark)
                fnt = ImageFont.truetype(WATERMARK["font_path"], size=50)
                text_width, text_height = draw.textsize(WATERMARK["link"], fnt)
                author_width, author_height = draw.textsize(
                    str(message.author)[:-2], fnt
                )
                # print(text_width)

                if size[0] < text_width or img_to_watermark.getpixel(
                    (size[0] - 1, size[1] - 1)
                ) == (1, 2, 1):
                    buf = io.BytesIO()
                    img_to_watermark.save(buf, format="PNG")
                    return buf.getvalue()

                if size[0] > text_width + author_width + 60:
                    paste_av = True
                    draw.text(
                        (60, (100 - text_height) // 2),
                        str(message.author)[:-2],
                        "WHITE",
                        fnt,
                    )

                draw.text(
                    (size[0] - text_width, (100 - text_height) // 2),
                    WATERMARK["link"],
                    "WHITE",
                    fnt,
                )

                new_size = (size[0], size[1] + 100)
                img_to_paste = Image.new("RGB", new_size)
                img_to_paste.paste(img_to_watermark, (0, 0))
                img_to_paste.paste(watermark, (0, size[1]))
                if paste_av:
                    img_to_paste.paste(avatar, (0, new_size[1] - 75))
                img_to_paste.putpixel((new_size[0] - 1, new_size[1] - 1), (1, 2, 1))

                buf = io.BytesIO()
                img_to_paste.save(buf, format="PNG")
                return buf.getvalue()

            async with aiohttp.ClientSession() as session_aio:
                async with session_aio.get(str(message.author.avatar_url)) as r:
                    if r.status == 200:
                        av_bytes = io.BytesIO(await r.read())

            images = {}
            # print(imgs)
            for name, image in imgs.items():
                image = Image.open(image).convert("RGB")
                size = image.size
                send = False
                if not (
                    len(imgs) == 1
                    and image.getpixel((size[0] - 1, size[1] - 1)) == (1, 2, 1)
                ):
                    send = True
                    avatar = Image.open(av_bytes).convert("RGB").resize((50, 50))
                    images[name] = watermark(image, avatar)

            global hooks
            try:
                hook_url = hooks[message.channel.id].url
            except KeyError:
                channel_hooks = await message.channel.webhooks()
                if not channel_hooks:
                    hook = await message.channel.create_webhook(name="zaGadka")
                else:
                    hook = channel_hooks[0]
                hooks[message.channel.id] = hook
                hook_url = hook.url

            if send:
                data = {
                    "content": " ".join(contents),
                    "avatar_url": str(message.author.avatar_url),
                    "username": str(message.author.nick)
                    # + "#"
                    # + str(message.author.discriminator)
                    if message.author.nick else str(message.author)[:-2],
                    "allowed_mentions": {"parse": []},
                }

                resp = requests.post(
                    hook_url, files=images, data={"payload_json": json.dumps(data)}
                )
                if resp.status_code == 200:
                    await message.delete()

    if not message.content:
        return

    if message.author.bot:
        return

    xd = message.guild.get_role(GUILD["xd_role_id"])
    if xd not in message.author.roles and (
        message.created_at - message.author.joined_at > timedelta(days=7)
    ):
        try:
            await message.author.add_roles(xd)
        except discord.errors.NotFound:
            return

    male, female = message.guild.get_role(GUILD["male"]), message.guild.get_role(
        GUILD["female"]
    )
    if (
        male not in message.author.roles
        and female not in message.author.roles
        and (message.created_at - message.author.joined_at > timedelta(minutes=1))
    ):
        if (
            "łem " in message.content.lower()
            or "lem " in message.content.lower()
            or "łbym " in message.content.lower()
            or "lbym " in message.content.lower()
        ):
            try:
                await message.author.add_roles(male)
            except discord.errors.NotFound:
                return
        elif (
            "łam " in message.content.lower()
            or "lam " in message.content.lower()
            or "łabym " in message.content.lower()
            or "labym " in message.content.lower()
        ):
            try:
                await message.author.add_roles(female)
            except discord.errors.NotFound:
                return

    fake_roles = [
        guild.get_role(GUILD["fake_everyone_id"]),
        guild.get_role(GUILD["fake_here_id"]),
    ]
    if any(role in message.role_mentions for role in fake_roles) and (
        fake_roles[0] not in author.roles
    ):
        await message.author.add_roles(*fake_roles, reason="everyone ping")

    args = message.content.split(" ")
    if len(message.content) < 2:
        return

    if (
        message.channel.id != GUILD["bots_channel_id"]
        and len(args[0]) > 1
        and args[0][0] in MUSIC_PREFIX
        and args[0][1:].lower() in MUSIC_COMMANDS
    ):
        try:
            await message.delete()
            return
        except discord.errors.NotFound:
            return

    for command in COMMANDS:
        if command in args[0].lower():
            return

    to_add = 0
    for role in BONUS_2.values():
        if role["id"] in member_roles_ids:
            to_add += role["points"]

    if len(args) > GUILD["max_message_points"]:
        points = (
            GUILD["max_message_points"] + 0.01 * GUILD["max_message_points"] * to_add
        )
    else:
        points = len(args) + 0.01 * len(args) * to_add
    no_ranking_role = message.guild.get_role(GUILD["no_ranking_role_id"])
    if no_ranking_role in message.author.roles:
        points = 0
    if isinstance(message.channel, discord.channel.VoiceChannel):
        points = 0

    if (check_member(message.author.id) is False) and (
        (date_now - message.author.joined_at).seconds > 60
    ):
        set_member(
            message.author.id,
            message.author.name,
            message.author.discriminator,
            None,
            datetime.now(),
            None,
            datetime.now(),
        )
        session.commit()
        set_member_scores(message.author.id, ["week"])
        session.commit()
    add_member_score(message.author.id, date_now.strftime("%A"), points)
    session.commit()

    parent_id = get_member_parent_id(message.author.id)

    if (
        parent_id
        and parent_id not in GUILD["inv_bots"]
        and parent_id != client.user.id
        and (check_member(parent_id))
        and (randint(1, 100) < GUILD["rand_parent"])
    ):
        add_member_score(parent_id, date_now.strftime("%A"), points)
    session.commit()

    if not message.attachments and message.content[0] == BOT["prefix"]:
        language = f.set_language(member)
        args = [x for x in args if x]
        command = args.pop(0)[1:].lower()

        print(
            datetime.now().strftime("%d/%m/%Y %H:%M:%S"), message.author, command, args
        )
        if command in ["reset_color", "rc", "resetcolor"]:
            for role in message.author.roles:
                if role.name == GUILD["multi_colored_name"]:
                    colors[role] = []
        if (command in ["color", "colour"]) and args:
            first_role = False
            try:
                color_string = "".join(args)
                if "#" in color_string:
                    color_string = color_string.replace("#", "")
                new_color = Color(color_string)
                print(new_color.hex_l)
                hex_string = new_color.hex_l.replace("#", "")
                discord_color = discord.Color(int(hex_string, 16))

                for role in message.author.roles:
                    if role.name == GUILD["multi_colored_name"]:
                        await role.edit(color=discord_color)
                        colors.setdefault(role, []).append(discord_color)
                        first_role = True
                    elif role.name == GUILD["colored_name"]:
                        await role.edit(colour=discord_color)
                        first_role = True
            except ValueError as err:
                print(err)
            try:
                color_string = "".join(args)
                if "#" in color_string:
                    color_string = color_string.replace("#", "")
                discord_color = discord.Color(int(color_string, 16))
                for role in message.author.roles:
                    if role.name == GUILD["multi_colored_name"]:
                        await role.edit(colour=discord_color)
                        colors.setdefault(role, []).append(discord_color)
                    elif role.name == GUILD["colored_name"]:
                        await role.edit(colour=discord_color)

            except ValueError:
                pass

        if command in ["help", "h"]:
            embed = discord.Embed()
            embed.add_field(
                name="connect permission",
                value="{}connect - <@{}>".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="view permission",
                value="{}view - <@{}>".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="speak permission",
                value="{}speak - <@{}>".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="reset permissions",
                value="{}reset <@{}>".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="global connect permission",
                value="{}connect -".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="global view permission",
                value="{}view -".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="global speak permission",
                value="{}speak -".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="reset global permissions",
                value="{}reset".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="user limit",
                value="{}limit 2".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.add_field(
                name="current channel",
                value="{}vc".format(BOT["prefix"], message.author.id),
                inline=True,
            )
            embed.set_footer(text="to allow permission use +")

            # await message.channel.send(embed=embed)
            await message.reply(embed=embed, mention_author=False)
            return
        if command in ["vc", "voicechat", "voicechannel"]:
            if member.voice and member.voice.channel:
                await message.reply(
                    f"<#{member.voice.channel.id}>", mention_author=False
                )
            else:
                await message.reply(
                    f"<#1324887489516671058>\n"  # create_public
                    f"<#1324888375378972823>\n"  # max2
                    f"<#1324888554773680158>\n"  # max3
                    f"<#1324888741151768637>\n"  # max4
                    f"<#1324888801750810735>",  # max5
                    mention_author=False,
                )

        if command in ["p", "profil", "profile"]:
            if args:
                author_id = args[0]
                author = f.get_member_from_string(author_id, message)
                try:
                    parent_id = get_member_parent_id(author.id)
                except AttributeError as err:
                    target_id = f.get_user_id_from_string(author_id)
                    ban_entry = await guild.fetch_ban(discord.Object(target_id))
                    await message.reply(
                        f"banned `{ban_entry[1].name}`: <@{ban_entry[1].id}> - {ban_entry[0]}",
                        mention_author=False,
                    )
                    return
            else:
                author = message.author
            inviter = guild.get_member(parent_id)
            if inviter is None:
                inviter_name = "None"
            else:
                inviter_name = inviter.display_name
            invited_count = get_invited_count(author.id)
            invited_list = get_invited_list(author.id)
            real_count = 0
            for invited_id in invited_list:
                invited = message.guild.get_member(invited_id)
                if (
                    invited
                    and invited.avatar
                    and invited.joined_at
                    and (invited.joined_at - invited.created_at)
                    > timedelta(days=GUILD["join_days"])
                ):
                    real_count += 1
            roles_l = [role.name for role in author.roles]
            roles_l.pop(0)
            roles = " ".join(roles_l) if len(roles_l) > 0 else "-"
            embed = discord.Embed()
            embed.set_author(name=author.name, icon_url=author.avatar_url)
            embed.add_field(
                name="created at",
                value=author.created_at.strftime("%d/%m/%Y\n%H:%M:%S"),
            )
            embed.add_field(
                name="joined at", value=author.joined_at.strftime("%d/%m/%Y\n%H:%M:%S")
            )
            embed.add_field(name="roles", value=roles)
            embed.add_field(
                name="invited people",
                value="{}({})".format(real_count, invited_count),
                inline=True,
            )
            embed.set_footer(text="invited by {}".format(inviter_name))
            # await message.channel.send(embed=embed)
            await message.reply(embed=embed, mention_author=False)
            return
        if command in ["leaveteam", "teamleave"]:
            for role in message.author.roles:
                if (GUILD["guild_name"] in role.name) and args:
                    team_owner = f.get_member_from_string(args.pop(0), message)
                    if not team_owner:
                        break
                    team_owner_roles_ids = [
                        role_owner.id for role_owner in team_owner.roles
                    ]
                    if all(
                        x in team_owner_roles_ids
                        for x in [role.id, GUILD["patreon_8_id"]]
                    ) and (team_owner.id != message.author.id):
                        await message.author.remove_roles(role)

        if command in ["speak", "s", "connect", "c", "view", "v", "reset", "r"]:
            if not f.check_if_has_permissions(message):
                try:
                    await message.reply(
                        message.author.mention + msg.VC_PERMISSION_MESSAGE[language],
                        mention_author=False,
                    )
                except AttributeError:
                    pass
                return
            if (len(args) < 1) and command not in ["reset", "r"]:
                return
            host = message.author
            if host.id not in channels.keys():
                for channel in guild.voice_channels:
                    if (
                        channel.id not in channels
                        and "-" + host.display_name == channel.name
                    ):
                        channels[channel.id] = host.id
            temp_channels = {k: v for k, v in channels.items() if v}
            temp_channels_tmp = {k: v for k, v in public_channels.items() if v}
            temp_channels_2 = {**temp_channels, **temp_channels_tmp}
            args = [x for x in args if x]

            if command in ["reset", "r"]:
                parameter = "+"
            elif args and (args[0] in ["+", "-"]):
                parameter = args.pop(0)
            else:
                parameter = "+"

            allowed = BOT["true"] + BOT["false"]
            if parameter not in allowed:
                parameter = True
            else:
                if parameter in BOT["true"]:
                    parameter = True
                elif parameter in BOT["false"]:
                    parameter = False
            target = args.pop(0) if args else None
            sex = (
                message.guild.get_role(GUILD[target.lower()])
                if target and target.lower() in ["male", "female"]
                else None
            )
            guest = (
                f.get_member_from_string(target, message)
                if not sex and target
                else None
            )

            afk_channel = message.guild.get_channel(GUILD["afk_channel_id"])
            if guest or sex:
                if guest:
                    overwrites = get_member_member(host.id, guest.id)
                    if not overwrites:
                        set_member_member(host.id, guest.id)
                        session.commit()
                        overwrites = get_member_member(host.id, guest.id)
                    view_channel, connect, speak = overwrites
                elif sex:
                    view_channel = connect = speak = None
                if command in ["reset", "r"]:
                    for channel_id in temp_channels_2:
                        channel = guild.get_channel(channel_id)
                        if not channel:
                            try:
                                channel = await client.fetch_channel(channel_id)
                            except discord.errors.NotFound:
                                if channel_id in channels:
                                    del channels[channel_id]
                                elif channel_id in public_channels:
                                    del public_channels[channel_id]
                                continue
                        if host.id == temp_channels_2[channel.id]:
                            if guest:
                                await channel.set_permissions(guest, overwrite=None)
                                if guest in channel.members:
                                    await guest.move_to(afk_channel)
                                    await guest.move_to(channel)
                            elif sex:
                                await channel.set_permissions(sex, overwrite=None)
                    if guest:
                        update_member_member(
                            host.id,
                            guest.id,
                            speak=None,
                            connect=None,
                            view_channel=None,
                        )
                        session.commit()
                elif command in ["speak", "s"]:
                    permission_overwrites = discord.PermissionOverwrite(
                        read_messages=view_channel, connect=connect, speak=parameter
                    )
                    for channel_id in temp_channels_2:
                        channel = guild.get_channel(channel_id)
                        if not channel:
                            try:
                                channel = await client.fetch_channel(channel_id)
                            except discord.errors.NotFound:
                                if channel_id in channels:
                                    del channels[channel_id]
                                elif channel_id in public_channels:
                                    del public_channels[channel_id]
                                continue
                        if host.id == temp_channels_2[channel.id]:
                            if guest:
                                await channel.set_permissions(
                                    guest, overwrite=permission_overwrites
                                )
                                try:
                                    if guest in channel.members:
                                        try:
                                            await guest.move_to(afk_channel)
                                            await guest.move_to(channel)
                                        except discord.errors.HTTPException:
                                            pass
                                except AttributeError:
                                    pass
                            elif sex:
                                await channel.set_permissions(
                                    sex, overwrite=permission_overwrites
                                )
                    if guest:
                        update_member_member(host.id, guest.id, speak=parameter)
                        session.commit()
                elif command in ["connect", "c"]:
                    permission_overwrites = discord.PermissionOverwrite(
                        read_messages=view_channel, connect=parameter, speak=speak
                    )
                    # temp_channels = {k: v for k, v in channels.items() if v}
                    for channel_id in temp_channels_2:
                        channel = guild.get_channel(channel_id)
                        if not channel:
                            try:
                                channel = await client.fetch_channel(channel_id)
                            except discord.errors.NotFound:
                                if channel_id in channels:
                                    del channels[channel_id]
                                elif channel_id in public_channels:
                                    del public_channels[channel_id]
                                continue
                        if host.id == temp_channels_2[channel.id]:
                            if guest:
                                # await channel.set_permissions(guest, connect=parameter)
                                await channel.set_permissions(
                                    guest, overwrite=permission_overwrites
                                )
                                if (parameter is False) and (guest in channel.members):
                                    await guest.move_to(afk_channel)
                            elif sex:
                                await channel.set_permissions(
                                    sex, overwrite=permission_overwrites
                                )
                    if guest:
                        update_member_member(host.id, guest.id, connect=parameter)
                        session.commit()
                elif command in ["view", "v"]:
                    permission_overwrites = discord.PermissionOverwrite(
                        read_messages=parameter, connect=connect, speak=speak
                    )
                    # temp_channels = {k: v for k, v in channels.items() if v}
                    for channel_id in temp_channels_2:
                        channel = guild.get_channel(channel_id)
                        if not channel:
                            try:
                                channel = await client.fetch_channel(channel_id)
                            except discord.errors.NotFound:
                                if channel_id in channels:
                                    del channels[channel_id]
                                elif channel_id in public_channels:
                                    del public_channels[channel_id]
                                continue
                        if host.id == temp_channels_2[channel.id]:
                            if guest:
                                # await channel.set_permissions(guest, view_channel=parameter)
                                await channel.set_permissions(
                                    guest, overwrite=permission_overwrites
                                )
                                if (parameter is False) and (guest in channel.members):
                                    await guest.move_to(afk_channel)
                            elif sex:
                                await channel.set_permissions(
                                    sex, overwrite=permission_overwrites
                                )
                    if guest:
                        update_member_member(host.id, guest.id, view_channel=parameter)
                        session.commit()
            else:
                if command in ["reset", "r"]:
                    create_channel = message.guild.get_channel(GUILD["create_channel"])
                    update_member(
                        host.id, speak=True, view_channel=True, connect=True, limit=99
                    )
                    update_member_members(host.id)
                    session.commit()
                    try:
                        await host.move_to(create_channel)
                    except discord.errors.HTTPException:
                        return
                elif command in ["speak", "s"]:
                    # temp_channels = {k: v for k, v in channels.items() if v}
                    for channel_id in temp_channels_2:
                        if host.id == temp_channels_2[channel_id]:
                            channel = guild.get_channel(channel_id)
                            if not channel:
                                try:
                                    channel = await client.fetch_channel(channel_id)
                                except discord.errors.NotFound:
                                    if channel_id in channels:
                                        del channels[channel_id]
                                    elif channel_id in public_channels:
                                        del public_channels[channel_id]
                                    continue

                            await channel.set_permissions(
                                message.guild.default_role, speak=parameter
                            )
                    update_member(host.id, speak=parameter)
                    session.commit()
                elif command in ["connect", "c"]:
                    for channel_id in temp_channels_2:
                        if host.id == temp_channels_2[channel_id]:
                            channel = guild.get_channel(channel_id)
                            if not channel:
                                try:
                                    channel = await client.fetch_channel(channel_id)
                                except discord.errors.NotFound:
                                    if channel_id in channels:
                                        del channels[channel_id]
                                    elif channel_id in public_channels:
                                        del public_channels[channel_id]
                                    continue
                            await channel.set_permissions(
                                message.guild.default_role, connect=parameter
                            )
                    update_member(host.id, connect=parameter)
                    session.commit()
                elif command in ["view", "v"]:
                    for channel_id in temp_channels_2:
                        if host.id == temp_channels_2[channel_id]:
                            channel = guild.get_channel(channel_id)
                            if not channel:
                                try:
                                    channel = await client.fetch_channel(channel_id)
                                except discord.errors.NotFound:
                                    if channel_id in channels:
                                        del channels[channel_id]
                                    elif channel_id in public_channels:
                                        del public_channels[channel_id]
                                    continue
                            await channel.set_permissions(
                                message.guild.default_role, view_channel=parameter
                            )
                    update_member(host.id, view_channel=parameter)
                    session.commit()

        if command in ["limit", "l"] and args:
            # if not f.check_if_has_permissions(message):
            #     await message.channel.send(
            #         message.author.mention +
            #         f' musisz byc patronem {LINKS["link_tree"]}, boosterem lub posiadac 4 zaproszone osoby')
            #     return
            host = message.author
            if host.id not in channels.keys():
                for channel in guild.voice_channels:
                    if (
                        channel.id not in channels
                        and "-" + host.display_name == channel.name
                    ):
                        channels[channel.id] = host.id
            limit_str = args.pop(0)
            if limit_str.isdigit():
                limit = int(limit_str)
                if isinstance(limit, int):
                    if limit < 0:
                        limit = 0
                    elif limit > 99:
                        limit = 0
            else:
                return
            temp_channels = {k: v for k, v in channels.items() if v}
            temp_channels_tmp = {k: v for k, v in public_channels.items() if v}
            temp_channels_2 = {**temp_channels, **temp_channels_tmp}

            for channel_id in temp_channels_2:
                if host.id == temp_channels_2[channel_id]:
                    channel = guild.get_channel(channel_id)
                    if not channel:
                        try:
                            channel = await client.fetch_channel(channel_id)
                        except discord.errors.NotFound:
                            if channel_id in channels:
                                del channels[channel_id]
                            elif channel_id in public_channels:
                                del public_channels[channel_id]
                            continue
                    await channel.edit(user_limit=limit)
            update_member(host.id, limit=limit)
            session.commit()

        if GUILD["patreon_8_id"] in [role.id for role in message.author.roles]:
            if command in ["teamname", "team_name", "tn"]:
                team_name = GUILD["guild_name"] + " " + " ".join(args)
                team_name_2 = " ".join(args) + GUILD["guild_name"]
                for role in message.author.roles:
                    if role.name.startswith(GUILD["guild_name"]):
                        await role.edit(name=team_name, reason="team name")
                        break
                team_category = message.guild.get_channel(GUILD["team_category"])
                for team_channel in team_category.text_channels:
                    if int(team_channel.topic) == message.author.id:
                        await team_channel.edit(name=team_name_2, reason="team name")
                        break
            if command in ["team"]:
                for role in message.author.roles:
                    if role.name.startswith(GUILD["guild_name"]):
                        parameter = args.pop(0)
                        if parameter in BOT["false"]:
                            parameter = False
                        else:
                            parameter = True
                        if message.mentions:
                            team_member = message.mentions[0]
                            if team_member.id == message.author.id:
                                return
                            if parameter:
                                if GUILD["patreon_8_id"] in [
                                    role.id for role in team_member.roles
                                ]:
                                    await message.reply("This user has his own team!")
                                    return
                                limit = 0
                                for limit_role_id in GUILD["limit_roles"]:
                                    if limit_role_id in [
                                        role.id for role in message.author.roles
                                    ]:
                                        limit += GUILD["team_limits"]
                                if len(role.members) < limit:

                                    def check(m):
                                        return (
                                            m.author.id == team_member.id
                                            and m.content.startswith(
                                                f'{BOT["prefix"]}join'
                                            )
                                        )

                                    await message.reply(
                                        f"{str(team_member)} wpisz `{BOT['prefix']}join` aby dołączyć do tego teamu, zaproszenie wygaśnie za 3 minuty.",
                                        mention_author=False,
                                    )
                                    await client.wait_for(
                                        "message", timeout=180.0, check=check
                                    )
                                    await team_member.add_roles(
                                        role, reason="add to team"
                                    )
                                else:
                                    await message.reply(
                                        "maximum number of members: {}".format(limit),
                                        mention_author=False,
                                    )
                            else:
                                await team_member.remove_roles(
                                    role, reason="remove from team"
                                )

        if GUILD["patreon_64_id"] in [role.id for role in message.author.roles]:
            print(args)
            if (command in ["teamcolor", "colorteam", "tc"]) and args:
                for role in author.roles:
                    if role.name.startswith(GUILD["guild_name"]):
                        print("1")
                        color_string = "".join(args).replace("#", "")

                        try:
                            new_color = Color(color_string)
                            print(new_color.hex_l)
                            hex_string = new_color.hex_l.replace("#", "")
                            discord_color = discord.Color(int(hex_string, 16))
                            await role.edit(color=discord_color)
                        except ValueError as err:
                            print(err)
                        try:
                            print("3")
                            discord_color = discord.Color(int(color_string, 16))
                            await role.edit(colour=discord_color)
                        except ValueError as err:
                            print(err)

        if GUILD["mod_1"] in [role.id for role in message.author.roles]:
            if command == "baninfo" and args:
                target_id = f.get_user_id_from_string(args[0])
                ban_entry = await guild.fetch_ban(discord.Object(target_id))
                await message.reply(
                    f"<@{ban_entry[1].id}> - {ban_entry[0]}", mention_author=False
                )

            if command == "male" and args:
                target = f.get_member_from_string(args.pop(0), message)
                male = message.guild.get_role(GUILD["male"])
                female = message.guild.get_role(GUILD["female"])

                if female in target.roles:
                    await target.remove_roles(female)

                await target.add_roles(male)

            if command == "female" and args:
                target = f.get_member_from_string(args.pop(0), message)
                female = message.guild.get_role(GUILD["female"])
                male = message.guild.get_role(GUILD["male"])

                if male in target.roles:
                    await target.remove_roles(male)
                await target.add_roles(female)

            if command == "dc" and message.mentions and message.mentions[0].bot:
                await message.mentions[0].move_to(None)
            if command in ["mutelive"]:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    await target.add_roles(
                        member.guild.get_role(GUILD["stream_mute_id"]),
                        reason=f"{message.author}: {message.content}",
                    )
                    muted = get_member_role(target.id, GUILD["stream_mute_id"])
                    if not muted:
                        set_member_role(
                            target.id, GUILD["stream_mute_id"], date=None, active=True
                        )
                        session.commit()
                    elif not muted.active:
                        update_member_role(
                            target.id, GUILD["stream_mute_id"], True, None
                        )
                        session.commit()

            if command in ["unmutelive"]:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    await target.remove_roles(
                        member.guild.get_role(GUILD["stream_mute_id"]),
                        reason=f"{message.author}: {message.content}",
                    )
                    muted = get_member_role(target.id, GUILD["stream_mute_id"])
                    if not muted:
                        pass
                    elif muted.active:
                        update_member_role(
                            target.id, GUILD["stream_mute_id"], False, None
                        )
                        session.commit()

            ###
            if command in ["muterank"]:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    await target.add_roles(
                        member.guild.get_role(GUILD["rank_mute_id"]),
                        reason=f"{message.author}: {message.content}",
                    )
                    muted = get_member_role(target.id, GUILD["rank_mute_id"])
                    if not muted:
                        set_member_role(
                            target.id, GUILD["rank_mute_id"], date=None, active=True
                        )
                        session.commit()
                    elif not muted.active:
                        update_member_role(target.id, GUILD["rank_mute_id"], True, None)
                        session.commit()

            if command in ["unmuterank"]:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    await target.remove_roles(
                        member.guild.get_role(GUILD["rank_mute_id"]),
                        reason=f"{message.author}: {message.content}",
                    )
                    muted = get_member_role(target.id, GUILD["rank_mute_id"])
                    if not muted:
                        pass
                    elif muted.active:
                        update_member_role(
                            target.id, GUILD["rank_mute_id"], False, None
                        )
                        session.commit()

            if command in ["clean", "prune", "clear"] and args:
                # target = args.pop(0).strip('<@!>')
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    trgt = target
                    try:
                        amount = int(args.pop(0))
                        if amount > 100:
                            amount = 100
                    except (IndexError, ValueError):
                        amount = 100
                    if trgt and (
                        trgt.top_role < message.author.top_role
                        or message.author.guild_permissions.administrator
                    ):
                        messages = []
                        async for mess in message.channel.history():
                            if (
                                mess.attachments
                                and mess.attachments[0].filename.startswith(
                                    f"discord-gg-zagadka_{target.id}"
                                )
                                and mess.author.bot
                            ) or mess.author == target:
                                messages.append(mess)
                        await message.channel.delete_messages(messages[:amount])
                    elif not trgt:
                        messages = []
                        async for mess in message.channel.history():
                            if (
                                mess.attachments
                                and mess.attachments[0].filename.startswith(
                                    f"discord-gg-zagadka_{target.id}"
                                )
                            ) or mess.author.id == int(target.id):
                                messages.append(mess)
                        await message.channel.delete_messages(messages[:amount])

            if command in ["cleanall", "pruneall", "clearall"] and args:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    trgt = target
                    print(trgt)
                    try:
                        amount = int(args.pop(0))
                        if amount > 100:
                            amount = 100
                    except (IndexError, ValueError):
                        amount = 100
                    if trgt and (
                        trgt.top_role < message.author.top_role
                        or message.author.guild_permissions.administrator
                    ):
                        for channel in guild.text_channels:
                            messages = []
                            async for mess in channel.history(limit=100):
                                if (
                                    mess.attachments
                                    and mess.attachments[0].filename.startswith(
                                        f"discord-gg-zagadka_{target.id}"
                                    )
                                    and mess.author.bot
                                ) or mess.author == target:
                                    messages.append(mess)
                            await channel.delete_messages(messages[:amount])
                    elif not trgt:
                        for channel in guild.text_channels:
                            messages = []
                            async for mess in channel.history(limit=100):
                                if (
                                    mess.attachments
                                    and mess.attachments[0].filename.startswith(
                                        f"discord-gg-zagadka_{target.id}"
                                    )
                                ) or mess.author.id == int(target.id):
                                    messages.append(mess)
                            await channel.delete_messages(messages[:amount])

            if command in ["cleanimg", "pruneimg", "clearimg"] and args:
                target = f.get_member_from_string(args.pop(0), message)
                if target:
                    trgt = target
                    print(trgt)
                    try:
                        amount = int(args.pop(0))
                        if amount > 100:
                            amount = 100
                    except (IndexError, ValueError):
                        amount = 100
                    if trgt and (
                        trgt.top_role < message.author.top_role
                        or message.author.guild_permissions.administrator
                    ):
                        messages = []
                        async for mess in message.channel.history():
                            if (
                                mess.attachments
                                and mess.attachments[0].filename.startswith(
                                    f"discord-gg-zagadka_{target.id}"
                                )
                                and mess.author.bot
                            ) or (
                                mess.author.id == int(target.id) and mess.attachments
                            ):
                                messages.append(mess)
                        await message.channel.delete_messages(messages[:amount])
                    elif not trgt:
                        messages = []
                        async for mess in message.channel.history():
                            if mess.attachments and mess.attachments[
                                0
                            ].filename.startswith(f"discord-gg-zagadka_{target.id}"):
                                messages.append(mess)
                        await message.channel.delete_messages(messages[:amount])
            if (
                command in ["b", "ban"]
                and (GUILD["mod_2"] not in [role.id for role in message.author.roles])
                and (not message.author.guild_permissions.administrator)
                and args
            ):
                to_ban_id = f.get_user_id_from_string(args.pop(0))
                if not args:
                    await message.reply("give reason!")
                    return
                to_ban = message.guild.get_member(to_ban_id)
                if to_ban:
                    if to_ban.guild_permissions.manage_messages:
                        await message.reply("dont ban moderator", mention_author=False)
                        return
                    if (datetime.now() - to_ban.joined_at) > timedelta(hours=2):
                        await message.reply("too long in server", mention_author=False)
                        return
                if not to_ban:
                    to_ban = discord.Object(to_ban_id)
                try:
                    await to_ban.send(
                        "Złamałeś zasady, ale możesz wrócić! Aby otrzymać unbana, wyślij 14,99 zł na <:mastercard:1270433919233425545> https://tipply.pl/u/zagadka, wpisując **TYLKO** swoje ID w polu 'Wpisz swój nick'. Po dokonaniu wpłaty, dołącz ponownie do serwera: https://discord.gg/zagadka. Twoje ID:"
                    )

                    await to_ban.send(to_ban.id)
                except:
                    pass
                try:
                    await message.guild.ban(
                        to_ban, reason=f'{message.author.name}: {" ".join(args)}'
                    )
                    await message.reply(
                        "<@{}> banned".format(to_ban_id), mention_author=False
                    )
                except discord.errors.NotFound:
                    await message.reply("error!")
            if command in ["ub", "unban"] and args:
                to_unban_id = f.get_user_id_from_string(args.pop(0))
                to_unban = discord.Object(to_unban_id)
                try:
                    await message.guild.unban(
                        to_unban, reason=f'{message.author.name}: {" ".join(args)}'
                    )
                    await message.reply(
                        "<@{}> unbanned!".format(to_unban_id), mention_author=False
                    )
                except discord.errors.NotFound:
                    await message.channel.send("error!")

        if GUILD["mod_2"] in [role.id for role in message.author.roles]:
            if (
                command in ["b", "ban"]
                and (not message.author.guild_permissions.administrator)
                and args
            ):
                to_ban_id = f.get_user_id_from_string(args.pop(0))
                if not args:
                    await message.reply("give reason!")
                    return
                to_ban = message.guild.get_member(to_ban_id)
                new_member = False
                if to_ban:
                    if to_ban.guild_permissions.manage_messages:
                        await message.reply("dont ban moderator", mention_author=False)
                        return
                    if (datetime.now() - to_ban.joined_at) < timedelta(hours=2):
                        new_member = True
                if not to_ban:
                    to_ban = discord.Object(to_ban_id)
                if not (author.id in daily_ban_limit):
                    daily_ban_limit[author.id] = GUILD["ban_limit"]
                if daily_ban_limit[author.id] > 0:
                    try:
                        await to_ban.send(
                            "Złamałeś zasady, ale możesz wrócić! Aby otrzymać unbana, wyślij 14,99 zł na <:mastercard:1270433919233425545> https://tipply.pl/u/zagadka, wpisując **TYLKO** swoje ID w polu 'Twój nick'. Po dokonaniu wpłaty, dołącz ponownie do serwera: https://discord.gg/zagadka. Twoje ID:"
                        )

                        await to_ban.send(to_ban.id)
                    except:
                        pass
                    try:
                        await message.guild.ban(
                            to_ban, reason=f'{message.author.name}: {" ".join(args)}'
                        )
                        await message.reply(
                            "<@{}> banned".format(to_ban_id), mention_author=False
                        )
                        if not new_member:
                            daily_ban_limit[author.id] -= 1
                    except discord.errors.NotFound:
                        await message.reply("error!")
            if command in ["unmod"]:
                # print(command)
                member_unmod = message.guild.get_member(
                    int(
                        args[0]
                        .replace("@", "")
                        .replace("<", "")
                        .replace(">", "")
                        .replace("!", "")
                    )
                )
                mod_role = message.guild.get_role(GUILD["mod_id"])
                # print(mod_role)
                if (
                    member_unmod
                    and not (member_unmod == message.author)
                    and mod_role in member_unmod.roles
                ):
                    await member_unmod.remove_roles(
                        mod_role, reason=str(message.author) + ": " "args"
                    )
                    # await asyncio.sleep(int(re.sub('[^0-9]', '', args[1])) * 86400)
                    # await member_unmod.add_roles(mod_role, reason=str(message.author) + ': ''args')
            if command in ["mod"]:
                member_mod = message.guild.get_member(
                    int(
                        args[0]
                        .replace("@", "")
                        .replace("<", "")
                        .replace(">", "")
                        .replace("!", "")
                    )
                )
                mod_role = message.guild.get_role(GUILD["mod_id"])
                if member_mod and not (member_mod == message.author):
                    await member_mod.add_roles(
                        mod_role, reason=str(message.author) + ": " "args"
                    )
            elif command == "resetpoints":
                if len(args) > 0:
                    if message.mentions:
                        reset_points_by_id(message.mentions[0].id)
                    else:
                        reset_points_by_id(args[0])
                else:
                    print(message.content)
                return
            if command in ["banall", "ban_all"] and (
                not message.author.guild_permissions.administrator
            ):
                if message.mentions:
                    inviter = message.mentions[0]
                else:
                    inviter_id = args.pop(0).strip("<@!>")
                    inviter = await client.fetch_user(inviter_id)
                temp_bans[inviter.id] = 2
                minutes = int(args.pop())
                if minutes > 60:
                    await message.reply("set value less than 60!")
                    return

                ban_list = get_invited_list_minutes(
                    inviter.id, datetime.now() - timedelta(minutes=minutes)
                )
                banned = await message.guild.bans()
                banned_ids = [banned_user[1].id for banned_user in banned]
                for member_id in ban_list:
                    member = discord.Object(member_id)
                    if member.id in banned_ids:
                        continue
                    try:
                        await message.channel.send("<@{}> banned".format(member_id))
                        await message.guild.ban(
                            member,
                            reason=f'{message.author.name} (ban_all): {" ".join(args)}',
                        )
                    except discord.errors.NotFound:
                        continue
        if message.author.guild_permissions.administrator:
            if command in ["duplicates"]:
                pass
            if command in ["refill"]:
                for mod_2 in daily_ban_limit:
                    daily_ban_limit[mod_2] = GUILD["ban_limit"]
            if command in ["b", "ban"] and args:
                to_ban_id = f.get_user_id_from_string(args.pop(0))
                to_ban = discord.Object(to_ban_id)
                try:
                    await message.guild.ban(
                        to_ban, reason=f'{message.author.name}: {" ".join(args)}'
                    )
                    await message.reply(
                        "<@{}> banned".format(to_ban_id), mention_author=False
                    )
                except discord.errors.NotFound:
                    await message.reply("error!")
            elif command == "everyone":
                await message.channel.send("@everyone")
                await message.delete()
            elif command == "here":
                await message.channel.send("@here")
                await message.delete()
            elif command == "everyone2":
                await message.channel.send("@everyone <@&759754258819448883>")
                await message.delete()
            elif command == "banner":
                if change_banner:
                    change_banner = False
                else:
                    change_banner = True
            if command in ["banall", "ban_all"]:
                if message.mentions:
                    inviter = message.mentions[0]
                else:
                    inviter_id = args.pop(0).strip("<@!>")
                    inviter = await client.fetch_user(inviter_id)
                temp_bans[inviter.id] = 2
                minutes = int(args.pop())

                ban_list = get_invited_list_minutes(
                    inviter.id, datetime.now() - timedelta(minutes=minutes)
                )
                banned = await message.guild.bans()
                banned_ids = [banned_user[1].id for banned_user in banned]
                for member_id in ban_list:
                    member = discord.Object(member_id)
                    if member.id in banned_ids:
                        continue
                    try:
                        await message.channel.send("<@{}> banned".format(member_id))
                        await message.guild.ban(
                            member,
                            reason=f'{message.author.name} (ban_all): {" ".join(args)}',
                        )
                    except discord.errors.NotFound:
                        continue
            if command in ["reactions"]:
                pass
                # await f.add_reactions(guild)
            if command in ["clearreactions", "creactions"]:
                pass
                # await f.add_reactions(guild, True)
            if command in ["testr"]:
                pass
                # await f.add_reactions(guild, check=True)
        if message.author.id == BOT["owner"]:
            if command in ["delcolors", "clearroles"]:
                await f.clear_roles(
                    guild,
                    True,
                    True,
                    (guild.get_role(GUILD["patreon_4_id"]),),
                    GUILD["colored_name"],
                    GUILD["multi_colored_name"],
                )
                await f.clear_roles(
                    guild,
                    False,
                    False,
                    (),
                    guild.get_role(GUILD["recruiter"]),
                    guild.get_role(GUILD["temp_bonus_id"]),
                )
            if command in ["resetroleperms"]:
                permission_overwrites = discord.Permissions(
                    embed_links=False,
                    attach_files=False,
                    change_nickname=False,
                    add_reactions=False,
                )
                for role in message.guild.roles:
                    print(role)
                    try:
                        await role.edit(permissions=permission_overwrites)
                    except discord.errors.Forbidden:
                        print("no perms")
            if command == "inv":
                channel = message.channel
                inv = await channel.create_invite()
                await message.channel.send(inv)
            if command == "new_date":
                print("New Date!")
                set_guild_date(guild.id, datetime.now() - timedelta(hours=23))
                bot_datetime = datetime.now() - timedelta(hours=23)
                session.commit()
            if command == "patreon":
                pass

            if command == "showall":
                if message.mentions:
                    inviter = message.mentions[0]
                else:
                    inviter_id = args.pop(0).strip("<@!>")
                    inviter = await client.fetch_user(inviter_id)
                ban_list = get_invited_list(inviter.id)
                for member_id in ban_list:
                    # member = message.guild.get_member(member_id)
                    member = discord.Object(member_id)
                    print(member)
                    try:
                        if member.id in [member.id for member in message.guild.members]:
                            await message.channel.send(
                                "<@{}> invited".format(member_id)
                            )
                        # await message.guild.ban(member)
                    except discord.errors.NotFound:
                        continue
            if command == "eval":
                # try:
                code = f.cleanup_code(" ".join(args))
                evaled = eval(code)
                # print(type(evaled))
                if inspect.isawaitable(evaled):
                    await message.channel.send(await evaled)
                else:
                    await message.channel.send("```{}```".format(evaled))
                # except:
                #     print('eval except')
                # if type(evaled) !=

            if command in ["invites"]:
                guild_invites = len(await guild.invites())
                await message.reply(str(guild_invites))

            if command in ["delinvcheck"] and args:
                uses = int(args.pop(0))
                count = 0
                for invite in await guild.invites():
                    if invite.uses <= uses:
                        count += 1
                        # await invite.delete()
                await message.reply(f"{count} will be deleted")

            if command in ["delinv"] and args:
                uses = int(args.pop(0))
                count = 0
                for invite in await guild.invites():
                    if invite.uses <= uses:
                        count += 1
                        await invite.delete()
                await message.reply(f"{count} will be deleted")

            if command == "datetime":
                print(date_now.strftime("%A"))
            elif command == "addNsfw":
                members = message.guild.members
                for member in members:
                    if (date_now - member.created_at).days < 14:
                        await member.add_roles(
                            message.guild.get_role(GUILD["nsfw_id"]),
                            reason="nsfw new account",
                        )
            elif command == "clearroles":
                for role in message.guild.roles:
                    if role.name in [
                        GUILD["colored_name"],
                        GUILD["multi_colored_name"],
                    ]:
                        await role.delete()
            elif command == "delTopRoles":
                top_roles = get_top_roles(128)
                for (role_id,) in top_roles:
                    print(role_id)
                    role = message.guild.get_role(role_id)
                    await role.delete()
                return
            elif command == "roles":
                role = message.guild.get_role(759754258819448883)
                for member in message.guild.members:
                    if len(member.roles) < 2:
                        try:
                            await member.add_roles(role, reason="xd")
                        except discord.errors.NotFound:
                            continue
            elif command == "say":
                await message.channel.send(" ".join(args))
                await message.delete()
            elif command == "sayhc":
                message_old = await message.channel.fetch_message(592443154058182686)
                content = msg.SAY_HC
                await message_old.edit(content=content)
            elif command == "rgb":
                message_rgb = await message.channel.send(GUILD["roles_rgb"])
                await message_rgb.delete()
                await message.delete()
            elif command == "editmessage":
                message_old = await message.channel.fetch_message(args.pop(0))
                content = " ".join(args)
                await message_old.edit(content=content)
            if command in ["category"]:
                joined = "".join(args)
                elements = joined.split(",")
                print(elements)
                print("categories: ", len(guild.categories))
                for category in guild.categories:
                    name = "".join(
                        ch
                        for ch in category.name.upper()
                        if (ch.isalnum() and ch != "٠" or ch == "_")
                    )
                    name_length = 0
                    for s in name:
                        if s in LETTER_LENGTH:
                            name_length += LETTER_LENGTH[s]
                    free_space = 100 - name_length
                    space_to_fill = free_space - 2 * 100 / int(elements[3])
                    amount = int(space_to_fill / 2 / (100 / int(elements[1])))
                    rounded = round(space_to_fill / 2 / (100 / int(elements[1])))
                    add_side = ""
                    if amount != rounded:
                        add_side = elements[0]
                    if elements[4] == "space":
                        space = " "
                    else:
                        space = ""
                    new_name = (
                        elements[2]
                        + add_side
                        + amount * elements[0]
                        + name
                        + amount * elements[0]
                        + space
                        + elements[2]
                    )
                    if new_name != category.name:
                        await category.edit(name=new_name)
                print("done!")


@client.event
async def on_member_remove(member):
    leave_logs = member.guild.get_channel(GUILD["leave_logs_id"])
    await leave_logs.send(
        "member: {}, display_name: {}".format(member.mention, member.display_name)
    )


@client.event
async def on_ready():
    date_now = datetime.now()
    guild = client.get_guild(GUILD["id"])
    try:
        invites.extend(await guild.invites())
    except:
        pass

    if check_score_by_type("week") is False:
        set_weeks()

    if check_role_by_type("top") is False:
        for i in range(1, GUILD["top"] + 1):
            print(i)
            role = await guild.create_role(name=i, hoist=True)
            set_role(role.id, i, "top")
            session.commit()

    if check_guild_by_id(GUILD["id"]) is False:
        set_guild_by_id(GUILD["id"], date_now)
    session.commit()

    print(client.user.id)
    print(client.user.name)
    print(discord.__version__)
    print("---------------")
    print("This bot is ready for action!")
    # client.loop.create_task(presence())


@client.event
async def on_invite_create(invite):
    # print('invite created')
    invites.append(invite)


@client.event
async def on_member_update(before, after):
    # print(after.activity)
    if after.id == 501689279676809216:
        log = True
    else:
        log = False
    # print(after.activities)
    member = after
    guild = after.guild
    member_role_ids = [role.id for role in after.roles]
    member_role_names = [role.name for role in after.roles]
    role_added = False
    # not member.pending \
    # and
    if (
        (GUILD["block_game"] not in member_role_ids)
        and (GUILD["xd_role_id"] in member_role_ids)
        and (after.activities != before.activities)
    ):
        # print(member.activities)
        if log:
            print(member.activities, before.activities)
        for activity in member.activities:
            try:
                game_type = activity.type
            except AttributeError as e:
                print(e)
                continue
            # print(game_type)
            # print(discord.ActivityType.playing)
            # game_id = activity.name.lower()
            if (
                (game_type != discord.ActivityType.playing)
                or (activity.name not in GAMES)
                or (GAMES[activity.name]["role"] in member_role_ids)
            ):
                continue
            else:
                # print(activity)
                for role in game_roles:
                    if role in member_role_ids:
                        try:
                            if member:
                                if log:
                                    print("remove_roles")
                                await member.remove_roles(
                                    guild.get_role(role), reason="remove game role"
                                )
                        except (discord.errors.NotFound, AttributeError) as err:
                            if log:
                                print(err)

                # del_game_roles = [guild.get_role(game['role']) for game in GAMES.values()]
                # try:
                #     await member.remove_roles(*del_game_roles, reason='remove game role')
                # except AttributeError:
                #     pass
                try:
                    if member:
                        if log:
                            print("add_role", activity.name, GAMES[activity.name])
                        await member.add_roles(
                            guild.get_role(GAMES[activity.name]["role"])
                        )
                        role_added = True
                    # print('game role!')
                except (discord.errors.NotFound, AttributeError) as err:
                    if log:
                        print(err)
                # print(activity)

    new_roles = list(set(after.roles) - set(before.roles))
    del_roles = list(set(before.roles) - set(after.roles))

    if len(new_roles) < 1 and len(del_roles) < 1 or role_added:
        return

    del_roles_ids = [role.id for role in del_roles]
    new_roles_ids = [role.id for role in new_roles]

    if GUILD["subscriber_1_id"] in new_roles_ids:
        await member.add_roles(
            guild.get_role(GUILD["patreon_2_id"]), reason="Subscriber 1"
        )
    elif GUILD["subscriber_1_id"] in del_roles_ids:
        await member.remove_roles(
            guild.get_role(GUILD["patreon_2_id"]), reason="Subscriber 1 removed"
        )

    if GUILD["subscriber_2_id"] in new_roles_ids:
        for role_id in [GUILD["patreon_2_id"], GUILD["patreon_4_id"]]:
            await member.add_roles(guild.get_role(role_id), reason="Subscriber 2")
    elif GUILD["subscriber_2_id"] in del_roles_ids:
        for role_id in [GUILD["patreon_2_id"], GUILD["patreon_4_id"]]:
            await member.remove_roles(
                guild.get_role(role_id), reason="Subscriber 2 removed"
            )

    if GUILD["subscriber_3_id"] in new_roles_ids:
        for role_id in [
            GUILD["patreon_2_id"],
            GUILD["patreon_4_id"],
            GUILD["patreon_8_id"],
        ]:
            await member.add_roles(guild.get_role(role_id), reason="Subscriber 3")
    elif GUILD["subscriber_3_id"] in del_roles_ids:
        for role_id in [
            GUILD["patreon_2_id"],
            GUILD["patreon_4_id"],
            GUILD["patreon_8_id"],
        ]:
            await member.remove_roles(
                guild.get_role(role_id), reason="Subscriber 3 removed"
            )

    language = f.set_language(member)
    if (
        GUILD["stream_mute_id"] in new_roles_ids
        or GUILD["stream_mute_id"] in del_roles_ids
    ) and member.voice:
        afk_channel = guild.get_channel(GUILD["afk_channel_id"])
        channel = member.voice.channel
        await member.move_to(afk_channel)
        await member.move_to(channel)

    mute_roles = [
        GUILD["nsfw_id"],
        GUILD["mute_id"],
        GUILD["stream_mute_id"],
        GUILD["rank_mute_id"],
    ]
    if any(x in new_roles_ids for x in mute_roles):
        try:
            msg_to_send = (
                "Złamałeś zasady! Aby otrzymać unmute i dodatkowe korzyści, wyślij 14,99 zł na "
                "<:mastercard:1270433919233425545> https://tipply.pl/u/zagadka, "
                "wpisując **TYLKO** swoje ID w polu 'Twój nick'.\n"
                "Sprawdź również inne dostępne opcje na kanale <#960665316109713421>.\n"
                "Twoje ID:"
            )

            await member.send(msg_to_send)
            await member.send(member.id)
        except:
            pass

    if GUILD["voter"] in del_roles_ids:
        try:
            await after.send(msg.VOTER_MESSAGES[language])
        except discord.errors.Forbidden as err:
            print(err)

    if any(x in [GUILD["nsfw_id"], GUILD["mute_id"]] for x in new_roles_ids):
        try:
            await member.edit(nick=None)
        except discord.errors.Forbidden:
            pass

        for color_role_id in GUILD["roles_colors"]:
            if color_role_id in member_role_ids:
                color_role = guild.get_role(color_role_id)
                await member.remove_roles(color_role)

    if GUILD["patreon_8_id"] in new_roles_ids:
        colored_role_position = guild.get_role(GUILD["colored_role_position"])
        # if not GUILD['multi_colored_name'] in member_role_names:
        #     colored_role = await guild.create_role(name=GUILD['multi_colored_name'])
        #     await member.add_roles(colored_role, reason='patreon $8')
        #     await colored_role.edit(position=colored_role_position.position + 1, reason='position')
        #
        #     if GUILD['colored_name'] in [role.name for role in member.roles]:
        #         colored_role_del = discord.utils.get(member.roles, name=GUILD['colored_name'])
        #         await colored_role_del.delete()
        have_team = False
        for role in member.roles:
            if role.name.startswith(GUILD["guild_name"]):
                await member.remove_roles(role, reason="remove from team")
        if not have_team:
            guild_role = await guild.create_role(
                name=f'{GUILD["guild_name"]} {member.name}'
            )
            await member.add_roles(guild_role, reason="patreon $8")
            await guild_role.edit(
                position=colored_role_position.position + 1, reason="position"
            )
            permission_overwrites = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                member: discord.PermissionOverwrite(manage_messages=True),
                guild_role: discord.PermissionOverwrite(read_messages=True),
            }
            team_category = guild.get_channel(GUILD["team_category"])
            new_channel = True
            for team_channel in team_category.text_channels:
                if int(team_channel.topic) == member.id:
                    new_channel = False
                    await team_channel.edit(overwrite=permission_overwrites)
                    break
            if new_channel:
                team_channel = await team_category.create_text_channel(
                    GUILD["guild_name"],
                    topic=member.id,
                    overwrites=permission_overwrites,
                )
                embed = discord.Embed()
                embed.set_author(name=member.name, icon_url=member.avatar_url)
                embed.add_field(
                    name="change team name",
                    value="{}teamname {}'s team".format(BOT["prefix"], member.name),
                    inline=True,
                )
                embed.add_field(
                    name="add teammate",
                    value="{}team + <@{}>".format(BOT["prefix"], member.id),
                    inline=True,
                )
                embed.add_field(
                    name="kick teammate",
                    value="{}team - <@{}>".format(BOT["prefix"], member.id),
                    inline=True,
                )
                message_to_pin = await team_channel.send(embed=embed)
                await message_to_pin.pin()

    if GUILD["patreon_4_id"] in new_roles_ids:
        if GUILD["patreon_8_id"] in member_role_ids:
            pass
        else:
            if not GUILD["colored_name"] in member_role_names:
                colored_role = await guild.create_role(name=GUILD["colored_name"])
                colored_role_position = guild.get_role(GUILD["colored_role_position"])
                await member.add_roles(colored_role, reason="patreon $4")
                await colored_role.edit(
                    position=colored_role_position.position + 1, reason="position"
                )
    if GUILD["patreon_2_id"] in new_roles_ids:
        if not GUILD["patreon_all_id"] in member_role_ids:
            default_patreon = guild.get_role(GUILD["patreon_all_id"])
            await member.add_roles(default_patreon, reason="patreon $2")


@client.event
async def on_raw_reaction_add(payload):
    pass
    # emoji = payload.emoji
    # # print(emoji)
    # if payload.member.id != client.user.id \
    #         and payload.channel_id in CHANNELS_REACTIONS \
    #         and payload.message_id in CHANNELS_REACTIONS[payload.channel_id] \
    #         and str(emoji) in CHANNELS_REACTIONS[payload.channel_id][payload.message_id]:
    #     member = payload.member
    #     guild = member.guild
    #     message_reaction_role = CHANNELS_REACTIONS[payload.channel_id][payload.message_id]
    #     role = guild.get_role(message_reaction_role[str(emoji)])
    #     nsfw = guild.get_role(GUILD['nsfw_id'])
    #     channel = member.guild.get_channel(payload.channel_id)
    #     async with channel.typing():
    #         message = await channel.fetch_message(payload.message_id)
    #         message_reactions = [str(r) for r in message.reactions]
    #         await message.remove_reaction(emoji, member)
    #         if not any(r in [role, nsfw] for r in member.roles):
    #             del_roles = []
    #             for role_id in message_reaction_role.values():
    #                 role_to_del = guild.get_role(role_id)
    #                 del_roles.append(role_to_del)
    #             await member.remove_roles(*del_roles, reason='reaction del')
    #             await member.add_roles(role, reason='reaction add')
    #         if len(message_reactions) != len(CHANNELS_REACTIONS[payload.channel_id][payload.message_id]):
    #             await message.clear_reactions()
    #             for reaction in CHANNELS_REACTIONS[payload.channel_id][payload.message_id]:
    #                 await message.add_reaction(reaction)


# @client.event
# async def on_guild_channel_create(channel):
#     if channel not in channel.guild.text_channels:
#         return
#     global hooks
#     await f.check_webhooks(channel.guild, WATERMARK['ignored_channels'], hooks)
#     # hooks.update(hooks_new)


if __name__ == "__main__":
    try:
        client.run(BOT["token"])
    except Exception as e:
        print("Could Not Start Bot")
        print(e)
    finally:
        print("Closing Session")
        session.close()
