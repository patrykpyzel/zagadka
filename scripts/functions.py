import io
import random
from collections.abc import Iterable
from itertools import chain
from typing import Iterable, List, Union

import discord
from mappings import CHANNELS_REACTIONS, GUILD, LINKS, WATERMARK
from PIL import Image, ImageDraw, ImageFont


async def clear_roles(
    guild: discord.Guild,
    by_name: bool = True,
    del_empty: bool = False,
    immune_roles: Iterable[discord.Role] = (),
    *roles_to_clear: Iterable[Union[str, discord.Role]],
) -> None:
    if by_name:
        roles_to_clear = [role for role in guild.roles if role.name in roles_to_clear]
    members = {member for role in roles_to_clear for member in role.members}
    for member in members:
        if any(r in member.roles for r in immune_roles):
            continue
        await member.remove_roles(*roles_to_clear)
    if del_empty:
        for role in roles_to_clear:
            if len(role.members) == 0:
                await role.delete()


async def watermark(img_to_watermark, message):
    size = img_to_watermark.size
    watermark = Image.new("RGB", (size[0], 100), "BLACK")
    draw = ImageDraw.Draw(watermark)
    fnt_big = ImageFont.truetype(WATERMARK["font_path"], size=40)
    fnt_small = ImageFont.truetype(WATERMARK["font_path"], size=20)
    text_width, text_height = draw.textsize(" .gg/zagadka", fnt_big)
    text2_width, text2_height = draw.textsize(
        "najaktywniejszy serwer w Polsce", fnt_small
    )
    author_width, author_height = draw.textsize(str(message.author), fnt_small)

    if size[0] < text_width + text2_width:  # można zrobić resize ale nie wiem czy warto
        buf = io.BytesIO()
        img_to_paste.save(buf, format="png")
        return buf.getvalue()

    draw.text(
        (size[0] - text_width - text2_width, (100 - text2_height) // 2),
        "najaktywniejszy serwer w Polsce",
        "WHITE",
        fnt_small,
    )
    draw.text(
        (size[0] - text_width, (100 - text_height) // 2),
        " .gg/zagadka",
        "WHITE",
        fnt_big,
    )
    if author_width < size[0] - text_width:
        draw.text(
            (0, (100 - text2_height) // 2), str(message.author), "WHITE", fnt_small
        )
    img_to_paste = Image.new("RGB", (size[0], size[1] + 100))
    img_to_paste.paste(img_to_watermark, (0, 0))
    img_to_paste.paste(watermark, (0, size[1]))

    buf = io.BytesIO()
    img_to_paste.save(buf, format="png")
    return buf.getvalue()


async def del_duplicates(member):
    role_names = [role.name for role in member.roles]
    color_count = role_names.count(GUILD["colored_name"])
    color_m_count = role_names.count(GUILD["multi_colored_name"])
    print(color_count, color_m_count)
    if sum([color_count, color_m_count]) < 2:
        return
    c = 0
    cm = 0
    for role in member.roles:
        if role.name == GUILD["colored_name"]:
            c += 1
            if color_m_count:
                await role.delete()
                continue
            elif c != color_count:
                await role.delete()

        elif role.name == GUILD["multi_colored_name"]:
            cm += 1
            if color_m_count != cm:
                await role.delete()


async def add_reactions(guild, clear=False, check=False):
    for channel_id, messages in CHANNELS_REACTIONS.items():
        channel = guild.get_channel(channel_id)
        for message_id in messages:
            message = await channel.fetch_message(message_id)
            if check:
                message_reactions = [str(r) for r in message.reactions]
                for r in messages[message_id]:
                    if r not in message_reactions:
                        await message.add_reaction(r)
            else:
                if clear:
                    await message.clear_reactions()
                for reaction in messages[message_id]:
                    await message.add_reaction(reaction)


def cleanup_code(content):
    """Automatically removes code blocks from the code."""
    # remove ```py\n```
    if content.startswith("```") and content.endswith("```"):
        return "\n".join(content.split("\n")[1:-1])

    # remove `foo`
    return content.strip("` \n")


def split_dict_equally(input_dict, chunks=2):
    return_list = [dict() for _ in range(chunks)]
    idx = 0
    for k, v in input_dict.items():
        return_list[idx][k] = v
        if idx < chunks - 1:  # indexes start at 0
            idx += 1
        else:
            idx = 0
    return return_list


async def check_webhooks(guild, ignored_channels, hooks_url):
    # hooks_url = {}
    for channel in guild.text_channels:
        # print(channel)
        if channel.id not in ignored_channels:
            hooks = await channel.webhooks()
            if not hooks:
                hook = await channel.create_webhook(name="zaGadka")
                hooks_url[channel.id] = hook
            else:
                hooks_url[channel.id] = hooks[0]
            print(hooks)
    # print(hooks_url)
    # return hooks_url


def check_if_has_permissions(message):
    if (
        (message.author not in message.author.guild.get_role(GUILD["join_id"]).members)
        and (
            message.author
            not in message.author.guild.get_role(GUILD["nitro_booster_id"]).members
        )
        and (
            message.author
            not in message.author.guild.get_role(GUILD["patreon_2_id"]).members
        )
    ):
        return False
    return True


def get_user_id_from_string(text):
    user_id = int(text.strip("<@!>-#"))
    return user_id


def get_member_from_string(text, message):
    if message.mentions:
        return message.mentions[0]
    else:
        return message.guild.get_member(get_user_id_from_string(text))


def set_language(member):
    member_role_names = [role.name for role in member.roles]
    if "🇬🇧" in member_role_names:
        language = "en"
    else:
        language = "pl"
    return language


def welcome_message(member, language):
    welcome_messages = {
        "en": [
            f"<@{member.id}> just joined the channel - glhf!",
            f"<@{member.id}> just joined. Everyone, look busy!",
            f"<@{member.id}> just joined. Can I get a heal?",
            f"<@{member.id}> joined your party.",
            f"<@{member.id}> joined. You must construct additional pylons.",
            f"Ermagherd. <@{member.id}> is here.",
            f"Welcome, <@{member.id}>. Stay awhile and listen.",
            f"Welcome, <@{member.id}>. We were expecting you ( ͡° ͜ʖ ͡°)",
            f"Welcome, <@{member.id}>. We hope you brought pizza.",
            f"Welcome <@{member.id}>. Leave your weapons by the door.",
            f"A wild <@{member.id}> appeared.",
            f"Swoooosh. <@{member.id}> just landed.",
            f"Brace yourselves. <@{member.id}> just joined the channel.",
            f"<@{member.id}> just joined. Hide your bananas.",
            f"<@{member.id}> just arrived. Seems OP - please nerf.",
            f"<@{member.id}> just slid into the channel.",
            f"A <@{member.id}> has spawned in the channel.",
            f"Big <@{member.id}> showed up!",
            f"Where’s <@{member.id}>? In the channel!",
            f"<@{member.id}> hopped into the channel. Kangaroo!!",
            f"<@{member.id}> just showed up. Hold my beer.",
            f"Challenger approaching - <@{member.id}> has appeared!",
            f"It's a bird! It's a plane! Nevermind, it's just <@{member.id}>.",
            f"It's <@{member.id}>! Praise the sun! [T]/",
            f"Never gonna give <@{member.id}> up. Never gonna let <@{member.id}> down.",
            f"Ha! <@{member.id}> has joined! You activated my trap card!",
            f"Cheers, love! <@{member.id}>'s here!",
            f"Hey! Listen! <@{member.id}> has joined!",
            f"We've been expecting you <@{member.id}>",
            f"It's dangerous to go alone, take <@{member.id}>!",
            f"<@{member.id}> has joined the channel! It's super effective!",
            f"Cheers, love! <@{member.id}> is here!",
            f"<@{member.id}> is here, as the prophecy foretold.",
            f"<@{member.id}> has arrived. Party's over.",
            f"Ready player <@{member.id}>",
            f"<@{member.id}> is here to kick butt and chew bubblegum. And <@{member.id}>",
            f"<@{member.id}> is all out of gum.",
            f"Hello. Is it <@{member.id}> you're looking for?",
            f"<@{member.id}> has joined. Stay a while and listen!",
            f"Roses are red, violets are blue, <@{member.id}> joined this channel with you",
        ],
        "pl": [
            f"<@{member.id}> dołączył do rozgrywki.",
            f"<@{member.id}> zwarty i gotowy do gry.",
            f"Powitajcie <@{member.id}> - nowego gracza.",
            f"<@{member.id}> zawitał, szykujcie się.",
            f"Hej, patrzcie! <@{member.id}> zaczął grać!",
            f"Wiwat! <@{member.id}> dołącza do gry!",
            f"Gotowy gracz <@{member.id}>",
            f"To ptak! To samolot! Oh nieważne, to tylko <@{member.id}>",
            f"Ha! <@{member.id}> dołączył! Aktywowałeś moją kartę pułapkę.",
            f"Astrologowie ogłaszają tydzień <@{member.id}>. Populacja graczy zwiększa się.",
            f"Cześć. Czy to <@{member.id}>, którego szukaliście?",
            f"Na górze róże, na dole fiołki, <@{member.id}> dołączył, zbijamy piątki",
            f"<@{member.id}> dołączył. Koniec imprezy.",
            f"Czekaliśmy na Ciebie <@{member.id}> ( ͡° ͜ʖ ͡°)",
            f"<@{member.id}> dołączył. Wygląda na mocnego.",
            f"<@{member.id}> jest tutaj. Zgodnie z przepowiednią.",
            f"Niebezpiecznie jest iść samemu. Weźmy <@{member.id}>!",
            f"<@{member.id}> pojawił się na kanale.",
            f"<@{member.id}> właśnie dołączył do kanału - glhf!",
            f"Witaj <@{member.id}>. Mamy nadzieję, że przyniosłeś pizzę.",
            f"Ziuuuum, <@{member.id}> właśnie wylądował.",
            f"<@{member.id}> wskoczył na kanał. Kangur!!",
            f"<@{member.id}> przybij pięć i do gierki pędź!",
            f"Dzień dobry <@{member.id}>, lepiej się przywitaj bo i tak Cię widzę.",
            f"<@{member.id}>, czy mnie jeszcze pamiętasz? Zagrajmy!",
            f"<@{member.id}>, co tak patrzysz? Daj buziaka i gramy!",
            f"<@{member.id}>, udanej gierki i dobrej bajerki!",
            f"<@{member.id}>, nic nie smakuje tak dobrze jak Twoje zwycięstwo",
            f"Dzień dobry <@{member.id}>, miłej gierki życzę!",
            f"Dzień dobry <@{member.id}>, jakby co to ja też już włączam gierkę.",
            f"Heej <@{member.id}>, zagramy razem w dolnej alei? 😳",
            f"Witaj <@{member.id}>, witaj bracie, w jaką gierkę dzisiaj gracie?",
            f"Czy już mówiłem, że <@{member.id}> jest super? 🥺",
            f"<@{member.id}>, ja Cię kręcę. Pięknie grasz!",
            f"<@{member.id}>, szalonej gierki!",
            f"No nie wytrzymam, znowu pros <@{member.id}> odpalił gierkę",
            f"Pamiętaj <@{member.id}>, jesteś dobrym graczem!",
            f"No cześć!!!!!!!!!!!!!! <@{member.id}>, lubię patrzeć jak grasz 😳",
            f"Buzi <@{member.id}>, miłej gierki 😘",
            f"Pełne skupienie <@{member.id}>, pokażemy jak się gra!",
            f"<@{member.id}>, potrzebujemy Cię w naszym składzie!",
            f"Bunkrów nie ma, ale jest <@{member.id}> i jest zajebiście!",
            f"Chrząszczyżewoszyce, powiat Łękołody, <@{member.id}> i gramy meczyk",
            f"<@{member.id}> nie będziesz dzwonił do swoich byłych fagasów z mojej komóry, bejbe, pakuj się i gramy.",
            f"Bohaterów <@{member.id}>, prądem? Odpalaj gierkę",
            f"Co Ty kurwa <@{member.id}> wiesz o graniu?",
            f"Wpuścić <@{member.id}> na salony...",
            f"Kaczka to max, co może z Ciebie być <@{member.id}>",
            f"Sąd sądem, ale sprawiedliwość musi być po naszej stronie. Gramy <@{member.id}>.",
            f"Żeby z <@{member.id}> nie móc w windzie...",
            f"Jak kochać to <@{member.id}>, jak kraść to miliony.",
            f"Łubu dubu, łubu dubu, niech żyje nam prezes naszego klubu"
            f"<@{member.id}>! Niech żyje nam! To śpiewałem ja - Oskarek",
            f"<@{member.id}>. Coco jamboo i do przodu!” – to moje hasło. Dobre, nie?",
            f"Memory... Find... <@{member.id}>. I wszystko jasne!",
            f"Gdzie mnie z tym szprajem, <@{member.id}> gamoniu!",
            f"<@{member.id}> Co za ponury absurd… Żeby o życiu decydować za młodu, kiedy jest się kretynem?",
            f"<@{member.id}> Jestem skrajnie wyczerpany, a przecież włączam pierwszą gierkę…",
            f"Żeby nie grać samotnie, gram z <@{member.id}>",
            f"<@{member.id}> Tu jest jakby luksusowo.",
            f"Otóż stryjeczny wuj szwagra mego drugiego męża <@{member.id}>, niesłychanie przystojny…",
            f"<@{member.id}> chciał mnie zabić! Jesteś taki sam jak twój ojciec!",
            f"Szczerze mówiąc, <@{member.id}>, nic mnie to nie obchodzi, gramy razem.",
            f"<@{member.id}> Zamierzam złożyć mu propozycję nie do odrzucenia. Gram z Tobą",
            f"Niech moc będzie z Tobą <@{member.id}>",
            f"<@{member.id}> Uwielbiam zapach napalmu o poranku.",
        ],
    }
    return random.choice(welcome_messages[language])
